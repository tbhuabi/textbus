import { jsx, jsxs, Fragment } from '@textbus/core'

export const jsxDEV = jsx

export {
  jsxs,
  Fragment
}
